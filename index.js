function findObject(obj, key, value) {
  var result, index, o

  if (typeof(obj) === 'array') {
    for (index in obj) {
      result = findObject(obj[index], key, value)

      if (result !== null) {
        return result
      }
    }

    return null
  }

  if (typeof(obj) !== 'object') {
    return null
  }

  if (obj && obj.hasOwnProperty(key) && obj[key] === value) {
    return obj
  } else {
    for (o in obj) {
      result = findObject(obj[o], key, value)

      if (result !== null) {
        return result
      }
    }
  }

  return null
}

module.exports = findObject